# Benchmarking the new --linked-reads WhatsHap command-line option

## Data

This uses data from the GIAB, see the `Snakefile` for URLs. Only chromosome 22
is phased.

Make an environment:

    conda create -n linkedreads python=3 hapcut2 pysam pyfaidx networkx biopython scipy xopen
    conda activate linkedreads

Then install WhatsHap into it

    ( cd wherever-whatshap-is && pip install -e . )

And run

    snakemake

## Notes

The reads are short, so connectivity is bad when *not* using `--linked-reads`:

      Best-case phasing would result in 4382 non-singleton phased blocks

When using `--linked-reads`, this goes down to 56. WhatsHap’s criteria for
putting two reads into the same read cloud:

- Must have same BX tag value
- Maximum distance between variants that are not on the same read is 50 kbp

HapCUT2 gives just 8 phase sets (blocks). The ground truth VCF has 285 blocks.


